<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\{
    Hash,Str
};
use App\Models\{
    User, Merchant
};
class RegisterController extends Controller
{
    public function register_customer(Request $req)
    {
        $user = new User($this->formData($req));
        if($user->save())
        {
            return $this->succesLoginResponse();
        }
        return $this->failedLoginResponse();
    }
    public function register_merchant(Request $req)
    {
        $merchant = new Merchant($this->formData($req));
        if($merchant->save())
        {
            return $this->succesLoginResponse();
        }
        return $this->failedLoginResponse();
    }
    public function formData($req)
    {
        return [
            'name'      => $req->name,
            'email'     => $req->email,
            'password'  => Hash::make($req->password),
            'no_hp'     => $req->no_hp,
            'alamat'    => $req->alamat,
            'api_token' => Str::random(60) 
        ];
    }
    private function failedLoginResponse()
    {
        return response()->json([
            'message'   => 'Registrasi gagal dilakukan',
        ],500);
    }
    private function successLoginResponse()
    {
        return response()->json([
            'message'   => 'Registrasi berhasil dilakukan',
            'data'      => [
                'api_token' => $user->api_token
            ] 
        ]);
}
}

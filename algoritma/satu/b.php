<?php
$angka = $argv[1];


$angka = $angka-1;
for ($i=1; $i <= $angka; $i++) 
{ 
    if ($i === 1 || $i === $angka) 
    {
        echo str_repeat('#',$angka);
        echo "\n";
    } else
    {
            $arr = range(1,$angka);

            $depan      = array_keys($arr,$i);
            $belakang   = array_keys(array_reverse($arr),$i);
            
            $replacer = [
                '0'             => '#',
                $depan[0]       => '#',
                $belakang[0]    => '#',
                count($arr)-1   => '#',
            ];

            $spasi = str_split(str_repeat(' ',$angka));

            echo implode('',array_replace($spasi,$replacer));
            echo "\n";
    }
}
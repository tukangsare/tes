<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Api\Auth\UserLoginController@login');
Route::post('merchant_login', 'Api\Auth\MerchantLoginController@login');

Route::post('register', 'Api\Auth\RegisterController@register_customer');
Route::post('merchant_register', 'Api\Auth\RegisterController@register_merchant');

Route::middleware('auth:api')->group(function () 
{
    Route::get('/products', 'Api\ProductController@index');
    Route::get('/product/{id}', 'Api\ProductController@find');
    Route::post('/transaction', 'Api\TransactionController@create');
});

Route::middleware('auth:merchant')->group(function () 
{
    Route::prefix('merchant')->group(function() 
    {
        Route::get('/products', 'Api\Merchant\ProductController@indexByMerchant');
        Route::get('/customers', 'Api\Merchant\CustomerController@index');
        Route::get('/product/{id}', 'Api\Merchant\ProductController@show');
        Route::get('/transactions', 'Api\Merchant\TransactionController@index');

    });
});
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'quantity', 'product_id', 'user_id', 'transaction_code'
    ];
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function product()
    {
        return $this->belongsTo(\App\Models\Product::class);
    }
    public function customer()
    {
        return $this->belongsTo(\App\Models\User::class,'user_id');
    }
}

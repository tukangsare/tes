<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'product_name', 'stock', 'product_description', 
    ];
    protected $hidden = [
        'created_at', 'updated_at', 'merchant_id'
    ];


    public function transactions()
    {
        return $this->hasMany(\App\Models\Transaction::class);
    }
    public function merchant()
    {
        return $this->belongsTo(\App\Models\Merchant::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Merchant extends Authenticatable
{
    use Notifiable;
    protected $guard = 'merchant';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'api_token', 'alamat', 'no_hp'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function products()
    {
        return $this->hasMany(\App\Models\Product::class);
    }
    public function loadCustomers()
    {
        $customers = \DB::table('users')
            ->join('transactions', 'users.id', '=', 'transactions.user_id')
            ->leftJoin('products', 'transactions.product_id', '=', 'products.id')
            ->leftJoin('merchants', 'products.merchant_id', '=', 'merchants.id')
            ->select('users.id','users.name','users.alamat')
            ->where('merchants.id',$this->id)
            ->distinct()
            ->get();
        return $customers;
    }
    public function loadTransactions()
    {
        $transactions = \DB::table('transactions')
            ->join('users', 'transactions.user_id', '=', 'users.id')
            ->leftJoin('products', 'transactions.product_id', '=', 'products.id')
            ->leftJoin('merchants', 'products.merchant_id', '=', 'merchants.id')
            ->select('transactions.*', 'products.product_name', 'users.name AS customer_name')
            ->where('merchants.id',$this->id)
            ->get();
        return $transactions;
    }
}

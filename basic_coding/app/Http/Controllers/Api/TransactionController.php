<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{
    Product, Transaction
};
use Illuminate\Support\Str;

class TransactionController extends Controller
{
    public function create(Request $req)
    {
        if(!$this->validateForm($req))
        {
            return response()->json(['Data yang dimasukkan kurang lengkap'],500);            
        }
        $user = \Auth::user();
        $data = $this->formData($req,$user);
        $transaction = new Transaction($data);
        if(!$transaction->save())
        {
            $this->stockUpdate($req->product,$req->qty);
            $this->pointUpdate($user);
            return response()->json(['Transaksi sukses, anda mendapatkan 1 point']);
        }
        return response()->json(['Transaksi gagal'],500);
    }
    private function validateForm($req)
    {
        if($this->validate($req, [
            'product'   => 'required',
            'qty'       => 'required'
        ])) {
            return true;
        } else
        {
            return false;
        }
    }
    public function formData($req,$user)
    {
        return [
            'transaction_code'  => Str::random(25),
            'product_id'        => $req->product,
            'quantity'          => $req->qty,
            'user_id'           => $user->id,
        ];
    } 
    public function stockUpdate($product,$qty)
    {
        $product = Product::find($product);
        $stockUpdate = $product->stock - $qty;
        return $product->update([
            'stock' => $stockUpdate
        ]);
    }
    public function pointUpdate($user)
    {
        $pointUpdate = $user->poin + 1;
        return $user->update([
            'poin' => $pointUpdate
        ]);
    }
}

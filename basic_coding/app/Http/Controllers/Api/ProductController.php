<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
class ProductController extends Controller
{
    public function index()
    {
        $products = Product::with('merchant:id,name')->get();
        return response()->json($products);
    }
    public function indexByMerchant()
    {
        $merchant = \Auth::user();
        $merchant->load('products');
        return response()->json($merchant->products);
    }
    public function find($id)
    {
        $product = Product::with('merchant:id,name')->find($id);
        return response()->json($product);
    }

}

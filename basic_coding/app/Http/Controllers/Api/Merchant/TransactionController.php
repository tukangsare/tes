<?php

namespace App\Http\Controllers\Api\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionController extends Controller
{
    public function index()
    {
        $merchant = \Auth::user();
        $transactions = $merchant->loadTransactions();
        return response()->json($transactions);
    }
}

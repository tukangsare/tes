<?php

namespace App\Http\Controllers\Api\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
class ProductController extends Controller
{
    public function indexByMerchant()
    {
        $merchant = \Auth::user();
        $merchant->load('products');
        return response()->json($merchant->products);
    }
    public function show($id)
    {
        $product = Product::with('transactions.customer:id,name')->find($id);
        return response()->json($product);
    }

}

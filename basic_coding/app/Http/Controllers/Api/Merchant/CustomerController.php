<?php

namespace App\Http\Controllers\Api\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;

class CustomerController extends Controller
{
    public function index()
    {
        $merchant = \Auth::user();
        $customers = $merchant->loadCustomers();
        return response()->json($customers);
    }
}

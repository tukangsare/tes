<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\Models\Merchant;

class MerchantLoginController extends Controller
{
    public function login(Request $req)
    {
        return $this->authenticate($req);
    }
    private function authenticate($credentials)
    {
        $user = Merchant::where('email', $credentials->email)->first();
        if($user && Hash::check($credentials->password, $user->password))
        {
            $user->update([
                'api_token' => Str::random(60)
            ]);
            return $this->successResponse($user);
        } else
        {
            return $this->failedResponse();
        }
    }
    public function successResponse($user)
    {
        return response()->json([
            'status'    => 'success',
            'message'   => 'Selamat',
            'data'      => [
                'token' => $user->api_token
            ]
        ]);
    }
    public function failedResponse()
    {
        response()->json([
            'status' => 'failed',
            'message' => 'User not found'
        ],401);
    }

}

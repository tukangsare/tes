<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserLoginController extends Controller
{
    public function login(Request $req)
    {
        $this->validateForm($req);
        $credentials = $req->only('email','password');
        $this->authenticate($credentials);
    }
    public function authenticate($credentials)
    {
        if(Auth::attempt($credentials)) 
        {
            $user->update([
                'api_token' => Str::random(60)
            ]);
            return $this->successResponse();
        } else
        {
            return $this->failedResponse();
        }
    }
    public function successResponse()
    {
        $user = Auth::user();
        return response()->json([
            'status'    => 'success',
            'message'   => 'Selamat',
            'data'      => [
                'token' => $user->api_token
            ]
        ]);
    }
    public function failedResponse()
    {
        response()->json([
            'status' => 'failed',
            'message' => 'User not found'
        ],401);
    }
    private function validateForm($req)
    {
        return $this->validate($req, [
            'email'     => 'required',
            'password'  => 'required'
        ]);
    }
}
